import java.util.Scanner;
public class Principal
{
    public static void main (String[] args) {
        Scanner le = new Scanner(System.in);  
        Aluno aluno = new Aluno();
        
        System.out.println("Insira o numero de Identificacao:");
        aluno.setNumeroIdentificacao(le.nextInt());
        
        System.out.println("Insira a primeira nota:");
        aluno.setNota1(le.nextDouble());
        
        System.out.println("Insira a segunda nota:");
        aluno.setNota2(le.nextDouble());
        
        System.out.println("Insira a terceira nota:");
        aluno.setNota3(le.nextDouble());
        
        System.out.println("Insira a media dos exercicios:");
        aluno.setMediaExercicios(le.nextDouble());
        
        System.out.println("A media de aproveitamento foi de " + aluno.calculaMediaAproveitamento()
        + " e o conceito e " + aluno.getConceito());
        
    }
  
}
