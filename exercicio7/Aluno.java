public class Aluno
{
    // instance variables - replace the example below with your own
    private int numeroIdentificacao;
    private double nota1;
    private double nota2;
    private double nota3;
    private double mediaExercicios;
    private String conceito;
    private double mediaAproveitamento;
    
    
    public Aluno()
    {
        
    }
    
    public int getNumeroIdentificacao() {
        return this.numeroIdentificacao;
    }
    
    public void setNumeroIdentificacao(int paramNumeroIdentificacao) {
        this.numeroIdentificacao = paramNumeroIdentificacao;
    }
    
    public double getNota1() {
        return this.nota1;
    }
    
    public void setNota1(double paramNota1) {
        this.nota1 = paramNota1;
    }
    
    public double getNota2() {
        return this.nota2;
    }
    
    public void setNota2(double paramNota2) {
        this.nota2 = paramNota2;
    }
    
    public double getNota3() {
        return this.nota3;
    }
    
    public void setNota3(double paramNota3) {
        this.nota3 = paramNota3;
    }
    
    public double getMediaExercicios() {
        return this.mediaExercicios;
    }
    
    public void setMediaExercicios(double paramMediaExercicios) {
        this.mediaExercicios = paramMediaExercicios;
    }
    
    public double calculaMediaAproveitamento() {
        this.mediaAproveitamento = (this.nota1 + (this.nota2 * 2) + (this.nota3 * 3)) / 7;
        return mediaAproveitamento;
    }
    
    public String getConceito() {
        if(this.mediaAproveitamento > 9) {
            return "A";
        } else if(this.mediaAproveitamento > 7.5 && this.mediaAproveitamento < 9) {
            return "B";
        } else if(this.mediaAproveitamento > 6 && this.mediaAproveitamento < 7.5) {
            return "C";
        } else if(this.mediaAproveitamento > 4 && this.mediaAproveitamento < 6) {
            return "D";
        } else {
            return "E";
        }
    }
    
}
